const content = {
  /* HERO */
  section_1: {
    name_1: "Christina",
    name_2: "Muhammet",
    card_title: "Приглашение на свадьбу",
    card_description: '7 января 2024 года, ресторан "Mon Cher", г. Оренбург.',
    wedding_date: "2024-01-07T15:30:00.999Z",
  },
  /* MAP */
  section_2: {
    title: "Прямо здесь!",
    coordinates: [51.827244, 55.13988],
    zoom_level: 15,
  },
  /* TIMING */
  section_3: {
    title: "Расписание мероприятия",
    card_1: {
      title: "15:30 - Сбор гостей",
      description:
        "В это время начинается наше главное мероприятие - сбор самых близких наих людей гостей! Найдите свое место и будьте готовы к нескончаемому веселью. Фуршет вам в этом поможет, не забудьте сделать фотографию 'ДО'",
    },
    card_2: {
      title: "16:00 - Церемония",
      description:
        "Время для серьезных моментов, которые перейдут в вечные воспоминания. Будем смеяться, плакать, вспоминать исторю знакомства: время для кринжовых шуток и неловких пауз. Особо чувствительные приготовьте носовые платочки для трогательного момента",
    },
    card_3: {
      title: "17:00 - Праздничный ужин",
      description:
        "Потрясающие вкусности и веселая компания ждут вас за столом. Приготовьтесь к кулинарному празднику и безудержному веселью! Веселимся, как никогда! Танцы, веселые конкурсы и неожиданные сюрпризы ждут вас.",
    },
  },
  /* DRESS-CODE */
  section_4: {
    title: "Дресс-код / Что надеть?",
    description:
      "Мы решили не ограничивать вас и себя в выборе цветовой палитры, вечерние наряды которые вам по душе и в которых вы сможете веселиться, радоваться и наслаждаться вечером. Black Tie Optional - приветствуется!",
    features: [
      {
        title: "Идея 1",
        description:
          "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying",
      },
      {
        title: "Идея 2",
        description:
          "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying",
      },
      {
        title: "Идея 3",
        description:
          "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying",
      },
    ],
  },
};

export default content;
