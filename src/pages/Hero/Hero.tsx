import {
  chakra,
  Stack,
  Box,
  useColorModeValue,
  Flex,
  Heading,
  Text,
  Image,
  useBreakpointValue,
} from "@chakra-ui/react";
import { useRef, useState } from "react";
import { useEffectOnce } from "../../hooks/use-effect-once";
import wings from "../../assets/wedding.png";
import content from "../../api/content";
import getTimeUntilDate from "../../utils/countdown";
import { FireworksHandlers } from "fireworks-js";
import Fireworks from "@fireworks-js/react";
const Hero = () => {
  const time = new Date(2024, 0, 7, 15, 30);
  const [until, setUntil] = useState({
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
  });
  const ref = useRef<FireworksHandlers>(null);

  useEffectOnce(() => {
    setInterval(() => {
      setUntil(getTimeUntilDate(time));
    }, 1000);

    setTimeout(() => {
      ref.current?.launch();
    }, 2000);
  });

  const display = useBreakpointValue({ sm: "block", base: "none" });
  const square_size = useBreakpointValue({
    base: "64px",
    sm: "86px",
    md: "128px",
  });
  const font_size = useBreakpointValue({
    base: "xl",
    sm: "2xl",
    md: "3xl",
  });
  const label_size = useBreakpointValue({
    base: "xs",
    sm: "sm",
    md: "md",
  });
  return (
    <>
      <Fireworks
        ref={ref}
        options={{ opacity: 0.5, intensity: 200, explosion: 3 }}
        style={{
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          position: "fixed",
        }}
      />
      <Box
        position="relative"
        zIndex={-2}
        pb={8}
        h="100vh"
        bg="url(hero-bottom.jpeg)"
        bgSize="cover"
      >
        <Stack
          zIndex={-1}
          pos="relative"
          bgGradient={`linear(to-l, blue.500, blue.400 , cyan.400)`}
          bgRepeat="no-repeat"
          bgImage="url(bg.jpg)"
          bgSize="cover"
          bgPos="center"
          height="50vh"
          w="100%"
        ></Stack>
        <Flex
          maxW="100%"
          isolation="isolate"
          justifyContent="center"
          zIndex={0}
          mt="-10rem"
          direction={["column", "column", "column", "row"]}
        >
          <Flex
            w={["100%", "100%", "100%", "fit-content"]}
            minW="256px"
            px={4}
            py={1}
            alignItems="center"
            bg="pink.100"
            roundedLeft={[0, 0, 0, "xl"]}
            direction="column"
            justifyContent="center"
          >
            <Heading size="xl">{content.section_1.name_1}</Heading>
            <Image src={wings} w="32px" />
            <Heading size="xl">{content.section_1.name_2}</Heading>
          </Flex>
          <Flex
            roundedRight={[0, 0, 0, "xl"]}
            boxShadow={useColorModeValue(
              "0 4px 6px rgba(160, 174, 192, 0.6)",
              "0 4px 6px rgba(9, 17, 28, 0.9)"
            )}
            bg="white"
            p={{ base: 4, sm: 8 }}
            overflow="hidden"
          >
            <Stack
              pos="relative"
              zIndex={0}
              direction="column"
              spacing={5}
              textAlign="left"
            >
              <chakra.h1 fontSize="4xl" lineHeight={1.2} fontWeight="bold">
                {content.section_1.card_title}
              </chakra.h1>
              <chakra.h1
                color="gray.400"
                fontSize="xl"
                maxW="600px"
                lineHeight={1.2}
              >
                {content.section_1.card_description}
              </chakra.h1>

              <Stack direction="row" spacing={3}>
                <Flex
                  justifyContent="center"
                  alignItems="center"
                  w={square_size}
                  h={square_size}
                  bg="gray.200"
                  rounded="xl"
                >
                  <Flex alignItems="center" direction="column">
                    <Heading fontSize={font_size}>{until.days}</Heading>
                    <Text fontSize={label_size} color="gray.500">
                      дней
                    </Text>
                  </Flex>
                </Flex>
                <Flex
                  justifyContent="center"
                  alignItems="center"
                  w={square_size}
                  h={square_size}
                  bg="gray.200"
                  rounded="xl"
                >
                  <Flex alignItems="center" direction="column">
                    <Heading fontSize={font_size}>{until.hours}</Heading>
                    <Text fontSize={label_size} color="gray.500">
                      часов
                    </Text>
                  </Flex>
                </Flex>
                <Flex
                  justifyContent="center"
                  alignItems="center"
                  w={square_size}
                  h={square_size}
                  bg="gray.200"
                  rounded="xl"
                >
                  <Flex alignItems="center" direction="column">
                    <Heading fontSize={font_size}>{until.minutes}</Heading>
                    <Text fontSize={label_size} color="gray.500">
                      минут
                    </Text>
                  </Flex>
                </Flex>
                <Flex
                  justifyContent="center"
                  alignItems="center"
                  w={square_size}
                  h={square_size}
                  bg="gray.200"
                  rounded="xl"
                >
                  <Flex alignItems="center" direction="column">
                    <Heading fontSize={font_size}>{until.seconds}</Heading>
                    <Text fontSize={label_size} color="gray.500">
                      секунд
                    </Text>
                  </Flex>
                </Flex>
              </Stack>
            </Stack>
          </Flex>
        </Flex>
      </Box>
    </>
  );
};

export default Hero;
