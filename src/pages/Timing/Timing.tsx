import {
  Box,
  chakra,
  Flex,
  SimpleGrid,
  Stat,
  StatLabel,
  StatNumber,
  useColorModeValue,
} from "@chakra-ui/react";
import content from "../../api/content";

interface StatsCardProps {
  title: string;
  stat: string;
}
function StatsCard(props: StatsCardProps) {
  const { title, stat } = props;
  return (
    <Stat
      px={{ base: 4, md: 8 }}
      py={"5"}
      shadow={"xl"}
      border={"1px solid"}
      borderColor={useColorModeValue("gray.800", "gray.500")}
      rounded={"lg"}
    >
      <StatLabel fontSize="2xl" fontWeight={600} isTruncated>
        {title}
      </StatLabel>
      <StatNumber mt={2} fontSize={"md"} fontWeight={"medium"}>
        {stat}
      </StatNumber>
    </Stat>
  );
}

export default function Timing() {
  return (
    <Flex
      direction="column"
      alignItems="center"
      maxW="7xl"
      mx={"auto"}
      mt={14}
      px={{ base: 2, sm: 12, md: 17 }}
    >
      <chakra.h2
        textAlign={"center"}
        fontSize={"3xl"}
        fontWeight={"bold"}
        background="pink.100"
        p={2}
        mb={14}
        display="block"
        w="fit-content"
      >
        {content.section_3.title}
      </chakra.h2>
      <SimpleGrid columns={{ base: 1, md: 3 }} spacing={{ base: 5, lg: 8 }}>
        <StatsCard
          title={content.section_3.card_1.title}
          stat={content.section_3.card_1.description}
        />
        <StatsCard
          title={content.section_3.card_2.title}
          stat={content.section_3.card_2.description}
        />
        <StatsCard
          title={content.section_3.card_3.title}
          stat={content.section_3.card_3.description}
        />
      </SimpleGrid>
    </Flex>
  );
}
