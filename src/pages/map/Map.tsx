import React from "react";
import { YMaps, Placemark, Map } from "@pbe/react-yandex-maps";
import { Flex, chakra } from "@chakra-ui/react";
import content from "../../api/content";
export default function SectionMap() {
  return (
    <Flex justifyContent="center" position="relative">
      <chakra.h2
        zIndex={99}
        background="pink.100"
        p={2}
        rounded="md"
        mt="1.5rem"
        position="absolute"
        fontSize="3xl"
        fontWeight={"bold"}
      >
        {content.section_2.title}
      </chakra.h2>
      <YMaps>
        <Map
          instanceRef={(ref) => {
            ref && ref.behaviors.disable("scrollZoom");
          }}
          height="50vh"
          width="100%"
          defaultState={{
            center: content.section_2.coordinates,
            zoom: content.section_2.zoom_level,
          }}
        >
          <Placemark geometry={content.section_2.coordinates} />
        </Map>
      </YMaps>
    </Flex>
  );
}
