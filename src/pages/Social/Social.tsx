"use client";

import {
  Box,
  chakra,
  Container,
  Flex,
  Heading,
  Link,
  Stack,
  Text,
  useColorModeValue,
  VisuallyHidden,
} from "@chakra-ui/react";
import {
  FaInstagram,
  FaTelegram,
  FaTwitter,
  FaWhatsapp,
  FaYoutube,
} from "react-icons/fa";
import { ReactNode } from "react";

const SocialButton = ({
  children,
  label,
  href,
}: {
  children: ReactNode;
  label: string;
  href: string;
}) => {
  return (
    <chakra.button
      bg={useColorModeValue("blackAlpha.100", "whiteAlpha.100")}
      rounded={"full"}
      w={8}
      h={8}
      cursor={"pointer"}
      as={"a"}
      href={href}
      display={"inline-flex"}
      alignItems={"center"}
      justifyContent={"center"}
      transition={"background 0.3s ease"}
      _hover={{
        bg: useColorModeValue("blackAlpha.200", "whiteAlpha.200"),
      }}
    >
      <VisuallyHidden>{label}</VisuallyHidden>
      {children}
    </chakra.button>
  );
};

export default function Social() {
  return (
    <Box
      pb={6}
      position="relative"
      zIndex={3}
      bg={useColorModeValue("gray.50", "gray.900")}
      color={useColorModeValue("gray.700", "gray.200")}
    >
      <Container
        as={Stack}
        maxW={"6xl"}
        py={4}
        spacing={4}
        justify={"center"}
        align={"center"}
      >
        <Heading fontSize="xl">
          Подтвердите свое присутствие на торжестве вступив в телеграмм канал
          или сообщением
        </Heading>
        <Stack direction={["column", "column", "column", "row"]} spacing={6}>
          <Link href="https://t.me/+vBYq_HjqmSQ1Yjgy">
            <Flex alignItems="center">
              <FaTelegram size={24} />
              <Text fontSize="xl" ml={2}>
                Организационная группа
              </Text>
            </Flex>
          </Link>
          <Link href="https://t.me/ChristinaPython">
            <Flex alignItems="center">
              <FaTelegram size={24} />
              <Text fontSize="xl" ml={2}>
                Маякнуть в личные сообщения
              </Text>
            </Flex>
          </Link>
          <Link href="https://wa.me/79033669796">
            <Flex alignItems="center">
              <FaWhatsapp size={24} />
              <Text fontSize="xl" ml={2}>
                Личка (WhatsApp)
              </Text>
            </Flex>
          </Link>
        </Stack>
      </Container>
    </Box>
  );
}
