"use client";

import {
  Box,
  VStack,
  Button,
  Flex,
  Divider,
  chakra,
  Grid,
  GridItem,
  Container,
  Heading,
  Text,
} from "@chakra-ui/react";
import content from "../../api/content";
import Carousel from "../../components/Carousel";

interface FeatureProps {
  heading: string;
  text: string;
}

const Feature = ({ heading, text }: FeatureProps) => {
  return (
    <GridItem>
      <chakra.h3 fontSize="xl" fontWeight="600">
        {heading}
      </chakra.h3>
      <chakra.p>{text}</chakra.p>
    </GridItem>
  );
};

export default function Dresscode() {
  return (
    <Flex
      direction={["column", "column", "column", "row"]}
      as={Container}
      maxW="7xl"
      mt={14}
      p={4}
    >
      <Flex
        maxW={["100%", "100%", "100%", "30%"]}
        mr={[0, 0, 0, 4]}
        mb={[4, 4, 4, 0]}
        direction="column"
      >
        <Heading p={2} bg="pink.100" fontSize="3xl">
          {content.section_4.title}
        </Heading>
        <Text mt={2} fontSize={{ base: "xl", md: "2xl" }}>
          {content.section_4.description}
        </Text>
      </Flex>
      <Carousel />
      {/* <Grid
        templateColumns={{
          base: "repeat(1, 1fr)",
          sm: "repeat(2, 1fr)",
          md: "repeat(2, 1fr)",
        }}
        gap={4}
      >
        <GridItem colSpan={1}>
          <VStack alignItems="flex-start" spacing="20px">
            <chakra.h2 p={2} bg="pink.100" fontSize="3xl" fontWeight="700">
              {content.section_4.title}
            </chakra.h2>
          </VStack>
        </GridItem>
        <GridItem>
          <Flex>
            <chakra.p fontSize={{ base: "xl", md: "2xl" }}>
              {content.section_4.description}
            </chakra.p>
          </Flex>
        </GridItem>
      </Grid> */}
    </Flex>
  );
}
