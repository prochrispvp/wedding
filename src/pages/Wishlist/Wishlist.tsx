"use client";

import {
  Avatar,
  Box,
  Flex,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import envelope from "../../assets/envelope.png";
import Carousel from "../../components/Carousel";
export default function Wishlist() {
  return (
    <Stack
      bg={useColorModeValue("gray.100", "gray.800")}
      py={16}
      px={8}
      spacing={{ base: 8, md: 10 }}
      align={"center"}
      direction={"column"}
    >
      <Box textAlign={"center"}>
        <Avatar src={envelope} mb={2} />

        <Text fontWeight={600} fontSize="3xl">
          Wish list
        </Text>
        <Text fontSize={"sm"} color={useColorModeValue("gray.400", "gray.400")}>
          конверт с деньгами!
        </Text>
      </Box>
      <Text
        fontSize={{ base: "xl", md: "2xl" }}
        textAlign={"center"}
        maxW={"3xl"}
      >
        Дорогие гости, вместо традиционных цветов мы бы предпочли бутылку вашего
        любимого напитка. Давайте создадим воспоминания, поднимая бокалы в вашу
        честь! Также если вы не знаете что нам подарить: "золотые монеты" и
        "денежные знаки" вполне подойдут для исполнения наших мечтаний)
      </Text>
    </Stack>
  );
}
