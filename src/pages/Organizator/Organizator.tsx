import { Fragment } from "react";
import {
  Container,
  Text,
  Stack,
  Avatar,
  Divider,
  Icon,
} from "@chakra-ui/react";
// Here we have used react-icons package for the icon
import julia from "../../assets/julia.jpg";
interface TestimonialAttributes {
  name: string;
  position: string;
  phone: string;
  content: string;
  image: string;
}

const testimonials: TestimonialAttributes[] = [
  {
    name: "Юлия Стрельникова",
    position: "Организатор",
    phone: "+7 922 540-60-88",
    image: julia,
    content: `Для любых вопросов и дополнительной информации, пожалуйста, обращайтесь к нашему чудесному организатору свадьбы.`,
  },
];

const Organizator = () => {
  return (
    <Container maxW="5xl" py={10} px={{ base: 5, md: 20 }}>
      {testimonials.map((obj, index) => (
        <Fragment key={index}>
          <Stack
            direction={{ base: "column", sm: "row" }}
            spacing={10}
            pt={1}
            justify="center"
          >
            <Avatar
              size="2xl"
              showBorder={true}
              borderColor="green.400"
              name="avatar"
              src={obj.image}
              display={{ base: "none", sm: "block" }}
            />

            <Stack direction="column" spacing={4} textAlign="left" maxW="4xl">
              {/* <Icon as={ImQuotesLeft} w={8} h={8} color="gray.400" /> */}
              <Text fontSize="md" fontWeight="medium">
                {obj.content}
              </Text>
              <Stack
                alignItems={{ base: "center", sm: "flex-start" }}
                spacing={0}
              >
                <Avatar
                  size="xl"
                  showBorder={true}
                  borderColor="green.400"
                  name="avatar"
                  src={obj.image}
                  display={{ base: "block", sm: "none" }}
                />
                <Text fontWeight="bold" fontSize="lg">
                  {obj.name}
                </Text>
                <Text fontWeight="medium" fontSize="sm" color="gray.600">
                  {obj.position}, {obj.phone}
                </Text>
              </Stack>
            </Stack>
          </Stack>
          {testimonials.length - 1 !== index && <Divider my={6} />}
        </Fragment>
      ))}
    </Container>
  );
};

export default Organizator;
