import { Container, Flex, Heading, Text } from "@chakra-ui/react";

export default function Welcome() {
  return (
    <Flex
      direction="column"
      alignItems="center"
      textAlign="center"
      as={Container}
      maxW="5xl"
      mt={2}
      p={4}
    >
      <Heading p={2} bg="pink.100" w="fit-content">
        Дорогие друзья!
      </Heading>
      <Text fontSize={{ base: "xl", md: "2xl" }} mt={4} mb={2}>
        Жить, любить, чувствовать. Однажды мы поняли, что нет ничего важнее
        этого. И что идти дальше мы хотим только вместе. А теперь мечтаем, чтобы
        день нашей свадьбы стал красивым и ярким событием на этом увлекательном
        пути.
      </Text>
      <Text fontSize={{ base: "xl", md: "2xl" }} mb={2}>
        Мы будем очень рады, если вы разделите этот счастливый день с нами.
      </Text>
      <Text fontSize={{ base: "xl", md: "2xl" }} mb={2}>
        Увидимся на нашей свадьбе!
      </Text>
    </Flex>
  );
}
