import "./App.css";
import Hero from "./pages/Hero/Hero";
import { Flex } from "@chakra-ui/react";
import SectionMap from "./pages/map/Map";
import Timing from "./pages/Timing/Timing";
import Dresscode from "./pages/Dresscode/Dresscode";
import Welcome from "./pages/Welcome/Welcome";
import Wishlist from "./pages/Wishlist/Wishlist";
import Organizator from "./pages/Organizator/Organizator";
import Social from "./pages/Social/Social";

function App() {
  return (
    <>
      <Flex direction="column">
        <></>
        <Hero />
        <Welcome />
        <SectionMap />
        <Timing />
        <Dresscode />
        <Wishlist />
        <Organizator />
        <Social />
      </Flex>
    </>
  );
}

export default App;
