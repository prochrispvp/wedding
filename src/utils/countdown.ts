export default function getTimeUntilDate(targetDate: Date): {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
} {
  const now = new Date();
  const timeDifference = targetDate.getTime() - now.getTime();

  if (timeDifference < 0) {
    // Если дата прошла, возвращаем все нули
    return { days: 0, hours: 0, minutes: 0, seconds: 0 };
  }

  const oneSecond = 1000;
  const oneMinute = oneSecond * 60;
  const oneHour = oneMinute * 60;
  const oneDay = oneHour * 24;

  const days = Math.floor(timeDifference / oneDay);
  const hours = Math.floor((timeDifference % oneDay) / oneHour);
  const minutes = Math.floor((timeDifference % oneHour) / oneMinute);
  const seconds = Math.floor((timeDifference % oneMinute) / oneSecond);

  return { days, hours, minutes, seconds };
}

// Пример использования функции:
const targetDate = new Date("2023-12-31T23:59:59");
const timeRemaining = getTimeUntilDate(targetDate);
